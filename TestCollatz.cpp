// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read0) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

TEST(CollatzFixture, read1) {
    ASSERT_EQ(collatz_read("100 200\n"), make_pair(100, 200));
}

TEST(CollatzFixture, read2) {
    ASSERT_EQ(collatz_read("201 210\n"), make_pair(201, 210));
}

TEST(CollatzFixture, read3) {
    ASSERT_EQ(collatz_read("900 1000\n"), make_pair(900, 1000));
}

TEST(CollatzFixture, read4) {
    ASSERT_EQ(collatz_read("10 1\n"), make_pair(10, 1));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(10, 1)), make_tuple(10, 1, 20));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(1, 1)), make_tuple(1, 1, 1));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(999999, 1)), make_tuple(999999, 1, 525));
}

// -----
// print
// -----

TEST(CollatzFixture, print0) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

TEST(CollatzFixture, print1) {
    ostringstream sout;
    collatz_print(sout, make_tuple(100, 200, 125));
    ASSERT_EQ(sout.str(), "100 200 125\n");
}

TEST(CollatzFixture, print2) {
    ostringstream sout;
    collatz_print(sout, make_tuple(201, 210, 89));
    ASSERT_EQ(sout.str(), "201 210 89\n");
}

TEST(CollatzFixture, print3) {
    ostringstream sout;
    collatz_print(sout, make_tuple(900, 1000, 174));
    ASSERT_EQ(sout.str(), "900 1000 174\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve0) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}

TEST(CollatzFixture, solve1) {
    istringstream sin("10 1\n200 100\n210 201\n1000 900\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n");
}

TEST(CollatzFixture, solve2) {
    istringstream sin("2000 5000\n10000 20000\n50000 100000\n500000 900000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "2000 5000 238\n10000 20000 279\n50000 100000 351\n500000 900000 525\n");
}

TEST(CollatzFixture, solve3) {
    istringstream sin("5000 2000\n20000 10000\n100000 50000\n900000 500000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "5000 2000 238\n20000 10000 279\n100000 50000 351\n900000 500000 525\n");
}
