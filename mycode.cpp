#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
#include <unordered_map> 
#include <sstream>
#include <cassert>

using namespace std;

unordered_map<int, int> dp;
int maxes[43][2] = {
        {837799, 525},
        {626331, 509},
        {511935, 470},
        {410011, 449},
        {230631, 443},
        {216367, 386},
        {156159, 383},
        {142587, 375},
        {106239, 354},
        {77031, 351},
        {52527, 340},
        {35655, 324},
        {34239, 311},
        {26623, 308},
        {23529, 282},
        {17647, 279},
        {13255, 276},
        {10971, 268},
        {6171, 262},
        {3711, 238},
        {2919, 217},
        {2463, 209},
        {2223, 183},
        {1161, 182},
        {871, 179},
        {703, 171},
        {649, 145},
        {327, 144},
        {313, 131},
        {231, 128},
        {171, 125},
        {129, 122},
        {97, 119},
        {73, 116},
        {54, 113},
        {27, 112},
        {25, 24},
        {18, 21},
        {9, 20},
        {7, 17},
        {6, 9},
        {3, 8},
        {2, 2}};

int brute(long num){
    assert(0 < num);
    if(dp.find(num) != dp.end())
        return dp[num];
    int result;
    if(num % 2 == 0)
        result = brute(num / 2) + 1;
    else
        result = brute(3 * num + 1) + 1;
    dp[num] = result; 
    return result;
}

int collatz(string line) {
    int low = 0;
    int high = 0;
    
    stringstream stream(line);
    stream >> low;
    stream >> high;
    
    assert(0 < low);
    assert(0 < high);
    assert(low < 1000000);
    assert(high < 1000000);
    
    if(low > high){
        low = low ^ high;
        high = low ^ high;
        low = low ^ high;
    }

    for(auto max : maxes)
        if(low <= max[0] && max[0] <= high)
            return max[1];
    
    int max = -1;
    for(int i = low; i <= high; i++){
        int result = brute(i);
        if(result > max)
            max = result;
    }

    assert(0 < max);
    assert(max <= 525);
    
    return max;
}

int main() {
    dp[1] = 1;
    dp[2] = 2;
    dp[3] = 8;
    dp[4] = 3;
    dp[5] = 6;
    dp[6] = 9;
    dp[7] = 17;
    dp[8] = 4;
    dp[9] = 20;
    
    for (string line; getline(cin, line);)
        cout << line << " " << collatz(line) << endl;
    return 0;
}