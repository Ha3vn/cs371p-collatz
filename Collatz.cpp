// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair
#include <unordered_map>
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <vector>

#include "Collatz.hpp"

using namespace std;

// cache
unordered_map<int, int> dp = {
    {1, 1},
    {2, 2},
    {3, 8},
    {4, 3},
    {5, 6},
    {6, 9},
    {7, 17},
    {8, 4},
    {9, 20}
};

// pre-computed maxes
int maxes[43][2] = {
    {837799, 525},
    {626331, 509},
    {511935, 470},
    {410011, 449},
    {230631, 443},
    {216367, 386},
    {156159, 383},
    {142587, 375},
    {106239, 354},
    {77031, 351},
    {52527, 340},
    {35655, 324},
    {34239, 311},
    {26623, 308},
    {23529, 282},
    {17647, 279},
    {13255, 276},
    {10971, 268},
    {6171, 262},
    {3711, 238},
    {2919, 217},
    {2463, 209},
    {2223, 183},
    {1161, 182},
    {871, 179},
    {703, 171},
    {649, 145},
    {327, 144},
    {313, 131},
    {231, 128},
    {171, 125},
    {129, 122},
    {97, 119},
    {73, 116},
    {54, 113},
    {27, 112},
    {25, 24},
    {18, 21},
    {9, 20},
    {7, 17},
    {6, 9},
    {3, 8},
    {2, 2}
};

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

// code to brute force find max cycle length with caching
int brute(long num) {
    assert(0 < num);
    long original = num;
    int result = 0;
    while(true) {
        if(dp.find(num) != dp.end()) {
            dp[original] = dp[num] + result;
            return dp[original];
        }
        if(num % 2 == 0)
            num = num / 2;
        else
            num = 3 * num + 1;
        result += 1;
    }
}

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j, k, l;
    tie(i, j) = p;

    assert(0 < i);
    assert(0 < j);
    assert(i < 1000000);
    assert(j < 1000000);

    k = i;
    l = j;

    if(i > j) {
        i = i ^ j;
        j = i ^ j;
        i = i ^ j;
    }

    // checks if range is applicable for pre-computed maxes
    for(auto m : maxes)
        if(i <= m[0] && m[0] <= j)
            return make_tuple(k, l, m[1]);

    // otherwise brute force
    int max = -1;
    for(int n = i; n <= j; n++) {
        int result = brute(n);
        if(result > max)
            max = result;
    }

    assert(0 < max);
    assert(max <= 525);

    return make_tuple(k, l, max);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
