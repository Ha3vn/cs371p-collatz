# CS371p: Object-Oriented Programming Collatz Repo

* Name: Michael Liu

* EID: MML2924

* GitLab ID: @Ha3vn

* HackerRank ID: @MichaelLiu2

* Git SHA: c506b9212772500a269d1d6a4d124114ebb6596c

* GitLab Pipelines: https://gitlab.com/Ha3vn/cs371p-collatz/-/pipelines

* Estimated completion time: 5 Hours

* Actual completion time: 4 Hours

* Comments: N/A
